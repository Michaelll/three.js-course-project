import './style.css';
import * as THREE from 'three';
import gsap from 'gsap';
import * as dat from 'dat.gui';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';

const cursor = {
  x: 0,
  y: 0,
};
window.addEventListener('mousemove', (e) => {
  cursor.x = e.clientX / sizes.width - 0.5;
  cursor.y = -(e.clientY / sizes.height - 0.5);
})

// Canvas
const canvas = document.querySelector('canvas.webgl')



// Scene
const scene = new THREE.Scene()

const fog = new THREE.Fog('#262837', 1, 15);
scene.fog = fog;

/**
 * Textures
 */
const textureLoader = new THREE.TextureLoader();

const doorColorTexture = textureLoader.load('/textures/door/color.jpg');
const doorAlphaTexture = textureLoader.load('/textures/door/alpha.jpg');
const doorAmbientOcclusionTexture = textureLoader.load('/textures/door/ambientOcclusion.jpg');
const doorHeightTexture = textureLoader.load('/textures/door/height.jpg');
const doorNormalTexture = textureLoader.load('/textures/door/normal.jpg');
const doorMetalnessTexture = textureLoader.load('/textures/door/metalness.jpg');
const doorRoughnessTexture = textureLoader.load('/textures/door/roughness.jpg');

const brickColorTexture = textureLoader.load('/textures/bricks/color.jpg');
const brickAmbientOcclusionTexture = textureLoader.load('/textures/bricks/ambientOcclusion.jpg');
const brickNormalTexture = textureLoader.load('/textures/bricks/normal.jpg');
const brickRoughnessTexture = textureLoader.load('/textures/bricks/roughness.jpg');

const grassColorTexture = textureLoader.load('/textures/grass/color.jpg');
const grassAmbientOcclusionTexture = textureLoader.load('/textures/grass/ambientOcclusion.jpg');
const grassNormalTexture = textureLoader.load('/textures/grass/normal.jpg');
const grassRoughnessTexture = textureLoader.load('/textures/grass/roughness.jpg');

grassColorTexture.repeat.set(10, 10);
grassAmbientOcclusionTexture.repeat.set(10, 10);
grassNormalTexture.repeat.set(10, 10);
grassRoughnessTexture.repeat.set(10, 10);

grassColorTexture.wrapS = THREE.RepeatWrapping;
grassAmbientOcclusionTexture.wrapS = THREE.RepeatWrapping;
grassNormalTexture.wrapS = THREE.RepeatWrapping;
grassRoughnessTexture.wrapS = THREE.RepeatWrapping;

grassColorTexture.wrapT = THREE.RepeatWrapping;
grassAmbientOcclusionTexture.wrapT = THREE.RepeatWrapping;
grassNormalTexture.wrapT = THREE.RepeatWrapping;
grassRoughnessTexture.wrapT = THREE.RepeatWrapping;
/**
 * Lights
 */
const ambientLight = new THREE.AmbientLight('0xb9d5ff', 0.12);
scene.add(ambientLight);

const directionalLight = new THREE.DirectionalLight('0xb9d5ff', 0.12);
directionalLight.position.set(4, 5, -2);
scene.add(directionalLight);

const doorLight = new THREE.PointLight('#dd7d46', 1.5, 8);
doorLight.position.set(0, 2.2, 2.7);


/**
 * Object
 */
const house = new THREE.Group();

const walls = new THREE.Mesh(
    new THREE.BoxBufferGeometry(4, 2.5, 4),
    new THREE.MeshStandardMaterial({
      map: brickColorTexture,
      transparent: true,
      aoMap: brickAmbientOcclusionTexture, // требует трюк с аттрибутом - ниже
      displacementScale: 0.1,
      normalMap: brickNormalTexture,
      roughnessMap: brickRoughnessTexture,
    })
);
walls.geometry.setAttribute('uv2', new THREE.Float32BufferAttribute(walls.geometry.attributes.uv.array, 2)); // трюк для aoMap - объемность текстуры
walls.position.y = 2.5 / 2,
house.add(walls);

const roof = new THREE.Mesh(
    new THREE.ConeBufferGeometry(3.5, 1, 4),
    new THREE.MeshStandardMaterial({
        color: '#8f2c2e',
    })
);
roof.rotation.y = Math.PI * 0.25;
roof.position.y = 2.5 + 0.5;
house.add(roof);

const door = new THREE.Mesh(
    new THREE.PlaneBufferGeometry(2.2, 2.2, 100, 100),
    new THREE.MeshStandardMaterial({
        map: doorColorTexture,
        transparent: true,
        alphaMap: doorAlphaTexture, // избавляемся от лишних частей текстуры
        aoMap: doorAmbientOcclusionTexture, // требует трюк с аттрибутом - ниже
        displacementMap: doorHeightTexture,
        displacementScale: 0.1,
        normalMap: doorNormalTexture,
        metalnessMap: doorMetalnessTexture,
        roughnessMap: doorRoughnessTexture,
    })
);
door.geometry.setAttribute('uv2', new THREE.Float32BufferAttribute(door.geometry.attributes.uv.array, 2)); // трюк для aoMap - объемность текстуры
door.position.set(0, 1, 2.001);
house.add(door);

const bushGeometry = new THREE.SphereBufferGeometry(1, 16, 16);
const bushMaterial = new THREE.MeshStandardMaterial({color: '#1c9122'});

const bush1 = new THREE.Mesh(bushGeometry, bushMaterial);
bush1.scale.set(0.5, 0.5, 0.5);
bush1.position.set(1.4, 0.1, 2.2);

const bush2 = new THREE.Mesh(bushGeometry, bushMaterial);
bush2.scale.set(0.2, 0.2, 0.2);
bush2.position.set(.75, 0.1, 2.2);

const bush3 = new THREE.Mesh(bushGeometry, bushMaterial);
bush3.scale.set(0.3, 0.3, 0.3);
bush3.position.set(0.95, 0.2, 2.5);

const bush4 = new THREE.Mesh(bushGeometry, bushMaterial);
bush4.scale.set(0.25, 0.25, 0.25);
bush4.position.set(1.9, 0.2, 2.3);

const bush5 = new THREE.Mesh(bushGeometry, bushMaterial);
bush5.scale.set(0.5, 0.5, 0.5);
bush5.position.set(-1.4, 0.1, 2.2);

const bush6 = new THREE.Mesh(bushGeometry, bushMaterial);
bush6.scale.set(0.2, 0.2, 0.2);
bush6.position.set(-.75, 0.1, 2.2);

const bush7 = new THREE.Mesh(bushGeometry, bushMaterial);
bush7.scale.set(0.3, 0.3, 0.3);
bush7.position.set(-0.95, 0.2, 2.5);

const bush8 = new THREE.Mesh(bushGeometry, bushMaterial);
bush8.scale.set(0.25, 0.25, 0.25);
bush8.position.set(-1.9, 0.2, 2.3);

house.add(bush1, bush2, bush3, bush4, bush5, bush6, bush7, bush8);

const graves = new THREE.Group();
scene.add(graves);

const graveGeometry = new THREE.BoxBufferGeometry(0.6, 0.8, 0.2);
const graveMaterial = new THREE.MeshStandardMaterial({color: '#9fa19f'});

for (let i=0; i<= 50; i++) {
  const angle = Math.random() * Math.PI * 2;
  const radius = 4 + Math.random() * 6;
  const x = Math.sin(angle) * radius;
  const z = Math.cos(angle) * radius;

  const grave = new THREE.Mesh(graveGeometry, graveMaterial);
  grave.rotation.y = (Math.random() - 0.5) * 1;
  grave.rotation.z = (Math.random() - 0.5) * 1;
  grave.position.set(x, 0.3, z);
  grave.castShadow = true;
  graves.add(grave);
}
house.add(doorLight);
scene.add(house);

const earthGeometry = new THREE.PlaneBufferGeometry(50, 50, 100, 100)
const earthMaterial = new THREE.MeshStandardMaterial({ 
  map: grassColorTexture,
  transparent: true,
  aoMap: grassAmbientOcclusionTexture, // требует трюк с аттрибутом - ниже
  displacementScale: 0.8,
  normalMap: grassNormalTexture,
  roughnessMap: grassRoughnessTexture,
});
const earth = new THREE.Mesh(earthGeometry, earthMaterial);
earth.geometry.setAttribute('uv2', new THREE.Float32BufferAttribute(earth.geometry.attributes.uv.array, 2)); // трюк для aoMap - объемность текстуры
earth.rotation.x = -Math.PI / 2;

scene.add(earth);

/**
 * Ghosts
 */
const ghost1 = new THREE.PointLight('#ff00ff', 2, 3);
scene.add(ghost1);
ghost1.castShadow = true;

const ghost2 = new THREE.PointLight('#ffff00', 2, 3);
scene.add(ghost2);
ghost2.castShadow = true;

const ghost3 = new THREE.PointLight('#00ffff', 2, 3);
scene.add(ghost3);
ghost3.castShadow = true;
/**
 * Sizes
 */
const sizes = {
    width: window.innerWidth,
    height: window.innerHeight
}

/**
 * Resizer
 */
window.addEventListener('resize', () => {
  sizes.width = window.innerWidth;
  sizes.height = window.innerHeight;
  camera.aspect = sizes.width / sizes.height;
  camera.updateProjectionMatrix();
  renderer.setSize(sizes.width, sizes.height);
});
window.addEventListener('dblclick', () => {
  const fullscreenElement = document.fullscreenElement || document.webkitFullscreenElement;
  if(!fullscreenElement) {
    if(canvas.requestFullscreen) {
      canvas.requestFullscreen();
    } else if(canvas.webkitRequestFullscreen) {
      canvas.webkitRequestFullscreen();
    }
  } else {
    if(document.exitFullscreen) {
      document.exitFullscreen();
    } else if(document.webkitExitFullscreen) {
      document.webkitExitFullscreen();
    }
  }
});

/**
 * Camera
 */
const camera = new THREE.PerspectiveCamera(75, sizes.width / sizes.height);
camera.position.set(1,3,12);
scene.add(camera)
camera.lookAt(earth.position);

const controls = new OrbitControls(camera, canvas);
controls.enableDamping = true;

/**
 * Renderer
 */
const renderer = new THREE.WebGLRenderer({
    canvas: canvas
})
renderer.setSize(sizes.width, sizes.height);
renderer.render(scene, camera);
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));
renderer.setClearColor('#262837');
renderer.shadowMap.enabled = true;
renderer.shadowMap.type = THREE.PCFSoftShadowMap;


walls.castShadow = true;
bush1.castShadow = true;
bush2.castShadow = true;
bush3.castShadow = true;
bush4.castShadow = true;
bush5.castShadow = true;
bush6.castShadow = true;
bush7.castShadow = true;
bush8.castShadow = true;
directionalLight.castShadow = true;
doorLight.castShadow = true;

doorLight.shadow.mapSize.width = 256;
doorLight.shadow.mapSize.height = 256;
doorLight.shadow.camera.far = 7;

ghost1.shadow.mapSize.width = 256;
ghost1.shadow.mapSize.height = 256;
ghost1.shadow.camera.far = 7;

ghost2.shadow.mapSize.width = 256;
ghost2.shadow.mapSize.height = 256;
ghost2.shadow.camera.far = 7;

ghost3.shadow.mapSize.width = 256;
ghost3.shadow.mapSize.height = 256;
ghost3.shadow.camera.far = 7;

earth.receiveShadow = true;

/**
 * Animations
 */

let time = Date.now();
let clock = new THREE.Clock();

const tick = () => {
    const elapsedTime = clock.getElapsedTime();

    //Update Ghosts
    const ghost1Angle = elapsedTime * 0.5;
    ghost1.position.x = Math.cos(ghost1Angle) * 5;
    ghost1.position.z = Math.sin(ghost1Angle) * 5;
    ghost1.position.y = Math.sin(elapsedTime * 3);

    const ghost2Angle = -elapsedTime * 0.8;
    ghost2.position.x = Math.cos(ghost2Angle) * 9;
    ghost2.position.z = Math.sin(ghost2Angle) * 9;
    ghost2.position.y = Math.sin(elapsedTime * 2) + Math.sin(elapsedTime * 4.5);

    const ghost3Angle = elapsedTime * 0.2;
    ghost3.position.x = Math.cos(ghost3Angle) * (7 + Math.sin(elapsedTime * 0.32));
    ghost3.position.z = Math.sin(ghost3Angle) * (7 + Math.sin(elapsedTime * 0.5));
    ghost3.position.y = Math.sin(elapsedTime * 4) + Math.sin(elapsedTime * 1.5);

    controls.update();

    renderer.render(scene, camera);

    window.requestAnimationFrame(tick);
};

tick();