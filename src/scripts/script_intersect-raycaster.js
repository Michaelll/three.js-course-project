import './style.css';
import * as THREE from 'three';
import gsap from 'gsap';
import * as dat from 'dat.gui';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';

const cursor = {
  x: 0,
  y: 0,
};
window.addEventListener('mousemove', (e) => {
  cursor.x = e.clientX / sizes.width - 0.5;
  cursor.y = -(e.clientY / sizes.height - 0.5);
})

const gui = new dat.GUI();
// Canvas
const canvas = document.querySelector('canvas.webgl')

// Scene
const scene = new THREE.Scene()

/**
 * Object
 */
const group = new THREE.Group();
scene.add(group);

const geometry = new THREE.SphereBufferGeometry(0.5, 10, 10)
const material = new THREE.MeshBasicMaterial({ color: 0x00ff00 })
const mesh1 = new THREE.Mesh(geometry, material);
const mesh2 = new THREE.Mesh(geometry, material);
mesh2.position.set(-2,0,0);
const mesh3 = new THREE.Mesh(geometry, material);
mesh3.position.set(2,0,0);
scene.add(mesh1, mesh2, mesh3);

const raycaster = new THREE.Raycaster();

/**
 * Sizes
 */
const sizes = {
    width: window.innerWidth,
    height: window.innerHeight
}

/**
 * Resizer
 */
window.addEventListener('resize', () => {
  sizes.width = window.innerWidth;
  sizes.height = window.innerHeight;
  camera.aspect = sizes.width / sizes.height;
  camera.updateProjectionMatrix();
  renderer.setSize(sizes.width, sizes.height);
});
window.addEventListener('dblclick', () => {
  const fullscreenElement = document.fullscreenElement || document.webkitFullscreenElement;
  if(!fullscreenElement) {
    if(canvas.requestFullscreen) {
      canvas.requestFullscreen();
    } else if(canvas.webkitRequestFullscreen) {
      canvas.webkitRequestFullscreen();
    }
  } else {
    if(document.exitFullscreen) {
      document.exitFullscreen();
    } else if(document.webkitExitFullscreen) {
      document.webkitExitFullscreen();
    }
  }
});

const mouse = new THREE.Vector2();

window.addEventListener('mousemove', (e) => {
  mouse.x = e.clientX / sizes.width * 2 - 1;
  mouse.y = -(e.clientY / sizes.height) * 2 + 1;
});

window.addEventListener('click', (e) => {
  if (currentIntersect) {
    alert(`click on the sphere: ${currentIntersect.object.uuid}`);
  }
});

/**
 * Camera
 */
const camera = new THREE.PerspectiveCamera(75, sizes.width / sizes.height);
camera.position.set(0,0,3);
scene.add(camera)
camera.lookAt(0,0,0);

const controls = new OrbitControls(camera, canvas);
controls.enableDamping = true;

/**
 * Renderer
 */
const renderer = new THREE.WebGLRenderer({
    canvas: canvas
})
renderer.setSize(sizes.width, sizes.height);
renderer.render(scene, camera);
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));

/**
 * Animations
 */

let time = Date.now();
let clock = new THREE.Clock();

let currentIntersect = null;

const tick = () => {
    const elapsedTime = clock.getElapsedTime();

    mesh1.position.y = Math.sin(elapsedTime * 0.3) * 1.5;
    mesh2.position.y = Math.sin(elapsedTime * 0.8) * 1.5;
    mesh3.position.y = Math.sin(elapsedTime * 1.4) * 1.5;

    raycaster.setFromCamera(mouse,camera);
    const objectsToWatch = [mesh1, mesh2, mesh3];
    objectsToWatch.forEach(element => {
      element.material.color = new THREE.Color(0xffff00);
    });

    // const rayOrigin = new THREE.Vector3(-3,0,0); // начало вектора
    // const rayDirection = new THREE.Vector3(10,0,0); // конец вектора
    // rayDirection.normalize()
    // raycaster.set(rayOrigin, rayDirection); // построение вектора

    const intersects = raycaster.intersectObjects(objectsToWatch);
    if (intersects.length){
      if(currentIntersect === null) {
        console.log('mouse enter');
      }
      currentIntersect = intersects[0];
      intersects.forEach(element => {
        element.object.material = material.clone();
        element.object.material.color = new THREE.Color(0xff00ff);
      });
    } else {
      if(currentIntersect) {
        console.log('mouse leave');
      }
      currentIntersect = null;
    }

    controls.update();

    renderer.render(scene, camera);

    window.requestAnimationFrame(tick);
};

tick();