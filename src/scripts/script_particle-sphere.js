import './style.css';
import * as THREE from 'three';
import gsap from 'gsap';
import * as dat from 'dat.gui';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';

const cursor = {
  x: 0,
  y: 0,
};
window.addEventListener('mousemove', (e) => {
  cursor.x = e.clientX / sizes.width - 0.5;
  cursor.y = -(e.clientY / sizes.height - 0.5);
})

// Canvas
const canvas = document.querySelector('canvas.webgl')

// Scene
const scene = new THREE.Scene()

/**
 * Particles
 */
const particleGeometry = new THREE.SphereBufferGeometry(1, 32, 32);
const particleMaterial = new THREE.PointsMaterial({
  size: 0.02,
  sizeAttenuation: false,
});

const particles = new THREE.Points(particleGeometry, particleMaterial);
// scene.add(particles);

const pointGeo = new THREE.SphereBufferGeometry(0.05, 32, 32);

function getRandomColor() {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

function randomInteger(min, max) {
  let rand = min + Math.random() * (max - min);
  return rand;
}

for (let i = 0; i < 100; i++) {
  const pointMat = new THREE.MeshBasicMaterial({ color: '#ffffff' });
  const point = new THREE.Mesh(pointGeo, pointMat);
  const color = getRandomColor();
  point.material.color = new THREE.Color(color);
  const radius = 1;
  const randomPoint1 = randomInteger(0, 2*Math.PI);
  const randomPoint2 = randomInteger(0, 2*Math.PI);
  point.position.x =  radius * Math.sin(randomPoint1) * Math.cos(randomPoint2);
  point.position.y =  radius * Math.sin(randomPoint1) * Math.sin(randomPoint2);
  point.position.z =  radius * Math.cos(randomPoint1);

    scene.add(point);
}

/**
 * Object
 */
const group = new THREE.Group();
scene.add(group);


/**
 * Sizes
 */
const sizes = {
    width: window.innerWidth,
    height: window.innerHeight
}

/**
 * Resizer
 */
window.addEventListener('resize', () => {
  sizes.width = window.innerWidth;
  sizes.height = window.innerHeight;
  camera.aspect = sizes.width / sizes.height;
  camera.updateProjectionMatrix();
  renderer.setSize(sizes.width, sizes.height);
});
window.addEventListener('dblclick', () => {
  const fullscreenElement = document.fullscreenElement || document.webkitFullscreenElement;
  if(!fullscreenElement) {
    if(canvas.requestFullscreen) {
      canvas.requestFullscreen();
    } else if(canvas.webkitRequestFullscreen) {
      canvas.webkitRequestFullscreen();
    }
  } else {
    if(document.exitFullscreen) {
      document.exitFullscreen();
    } else if(document.webkitExitFullscreen) {
      document.webkitExitFullscreen();
    }
  }
});

/**
 * Camera
 */
const camera = new THREE.PerspectiveCamera(75, sizes.width / sizes.height);
camera.position.set(0,0,3);
scene.add(camera)
camera.lookAt(0, 0, 0);

const controls = new OrbitControls(camera, canvas);
controls.enableDamping = true;

/**
 * Renderer
 */
const renderer = new THREE.WebGLRenderer({
    canvas: canvas
})
renderer.setSize(sizes.width, sizes.height);
renderer.render(scene, camera);
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));

/**
 * Animations
 */

let time = Date.now();
let clock = new THREE.Clock();

const tick = () => {
    const elapsedTime = clock.getElapsedTime();

    controls.update();

    renderer.render(scene, camera);

    window.requestAnimationFrame(tick);
};

tick();