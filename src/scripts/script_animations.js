import './style.css'
import * as THREE from 'three'
import gsap from 'gsap'

console.log(gsap);

// Canvas
const canvas = document.querySelector('canvas.webgl')

// Scene
const scene = new THREE.Scene()

/**
 * Object
 */
const group = new THREE.Group();
scene.add(group);

const geometry = new THREE.BoxGeometry(1, 1, 1)
const material = new THREE.MeshBasicMaterial({ color: 0x00ff00 })
const mesh = new THREE.Mesh(geometry, material);
scene.add(mesh);

/**
 * Sizes
 */
const sizes = {
    width: 1440,
    height: 768
}

/**
 * Camera
 */
const camera = new THREE.PerspectiveCamera(75, sizes.width / sizes.height);
camera.position.set(0,0,3);
scene.add(camera)

/**
 * Renderer
 */
const renderer = new THREE.WebGLRenderer({
    canvas: canvas
})
renderer.setSize(sizes.width, sizes.height)
renderer.render(scene, camera)

/**
 * Animations
 */

let time = Date.now();
let clock = new THREE.Clock();

// gsap.to(mesh.position, {duration: 3,delay: 1, x: 5, y: 15});
// gsap.to(mesh.position, {duration: 1,delay: 3, x: 0, y: 5});

const tick = () => {

    // const currentTime = Date.now();
    // const deltaTime = currentTime - time;
    // time = currentTime;

    const elapsedTime = clock.getElapsedTime();

    // camera.position.x = Math.cos(elapsedTime);
    // camera.position.y = Math.sin(elapsedTime);
    camera.lookAt(mesh.position);

    renderer.render(scene, camera);

    window.requestAnimationFrame(tick);
};

tick();