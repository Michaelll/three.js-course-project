import './style.css';
import * as THREE from 'three';
import gsap from 'gsap';
import * as dat from 'dat.gui';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';

const cursor = {
  x: 0,
  y: 0,
};
window.addEventListener('mousemove', (e) => {
  cursor.x = e.clientX / sizes.width - 0.5;
  cursor.y = -(e.clientY / sizes.height - 0.5);
})

const gui = new dat.GUI()

// Canvas
const canvas = document.querySelector('canvas.webgl')

// Scene
const scene = new THREE.Scene()

/**
 * Loaders
 */
const textureLoader = new THREE.TextureLoader();
const bakedShadow = textureLoader.load('/textures/shadows/bakedShadow.jpg');
const simpleShadow = textureLoader.load('/textures/shadows/simpleShadow.jpg');


/**
 * Sizes
 */
 const sizes = {
  width: window.innerWidth,
  height: window.innerHeight
}

/**
 * Renderer
 */
 const renderer = new THREE.WebGLRenderer({
  canvas: canvas
})
renderer.setSize(sizes.width, sizes.height);
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));
renderer.shadowMapEnabled = true;
renderer.shadowMapSoft = true;
renderer.shadowMap.type = THREE.PCFSoftShadowMap;
renderer.shadowMap.enabled = false;

/**
 * Object
 */
const group = new THREE.Group();
scene.add(group);

const cubeGeometry = new THREE.SphereGeometry(0.5, 42, 16);
const material = new THREE.MeshLambertMaterial({ color: 0xffffff });
const cube = new THREE.Mesh(cubeGeometry, material);
cube.receiveShadow = true;
cube.castShadow = true;
scene.add(cube);



const planeGeometry = new THREE.PlaneGeometry(4, 4);
const plane = new THREE.Mesh(planeGeometry, new THREE.MeshBasicMaterial({
    // map: bakedShadow,
    color: 0xfffff1,
}));
plane.rotation.x = -Math.PI / 2;
plane.position.y = -0.5;
plane.receiveShadow = true;
scene.add(plane);

const objShadow  = new THREE.Mesh(
    new THREE.PlaneBufferGeometry(1.5, 1.5),
    new THREE.MeshBasicMaterial({
        color: 0x000000,
        transparent: true,
        alphaMap: simpleShadow,
    })
);
objShadow.rotation.x = -Math.PI / 2;
objShadow.position.y =  plane.position.y + 0.001;
scene.add(objShadow);

/**
 * Lights
 */
 const ambientLight = new THREE.AmbientLight(0xffffff, 0.3);
 scene.add(ambientLight);

const directionalLight = new THREE.DirectionalLight(0xffffff, .3, 100);
directionalLight.position.set(2, 2.5, 2);
directionalLight.castShadow = true;
scene.add(directionalLight);

directionalLight.shadow.mapSize.width = 1024;
directionalLight.shadow.mapSize.height = 1024;
directionalLight.shadow.camera.top = 2;
directionalLight.shadow.camera.right = 2;
directionalLight.shadow.camera.bottom = -2;
directionalLight.shadow.camera.left = -2;
directionalLight.shadow.camera.near = 1;
directionalLight.shadow.camera.far = 6;
// directionalLight.shadow.radius = 10;

gui.add(directionalLight.position, 'x').min(-2).max(2).step(0.01);
gui.add(directionalLight.position, 'y').min(-2).max(2).step(0.01);
gui.add(directionalLight.position, 'z').min(-2).max(2).step(0.01);
gui.add(directionalLight, 'intensity').min(-2).max(2).step(0.01);

const directionalLightCameraHelper = new THREE.CameraHelper( directionalLight.shadow.camera );
scene.add(directionalLightCameraHelper);
directionalLightCameraHelper.visible = false;

const spotLight = new THREE.SpotLight(0xffffff, 0.3, 5, Math.PI * 0.3);
spotLight.castShadow = true;
spotLight.shadow.mapSize.width = 1024;
spotLight.shadow.mapSize.height = 1024;
spotLight.shadow.camera.fov = 30;
spotLight.shadow.camera.near = 1;
spotLight.shadow.camera.far = 6;

spotLight.position.set(0, 2, 2);
scene.add(spotLight);
scene.add(spotLight.target);

const spotLightCameraHelper = new THREE.CameraHelper( spotLight.shadow.camera );
scene.add(spotLightCameraHelper);
spotLightCameraHelper.visible = false;

const pointLight = new THREE.PointLight(0xffffff, 0.3);
pointLight.castShadow = true;
pointLight.shadow.mapSize.width = 1024;
pointLight.shadow.mapSize.height = 1024;
pointLight.shadow.camera.near = 0.1;
pointLight.shadow.camera.far = 5;

pointLight.position.set(-1, 1, 0);
scene.add(pointLight);

const pointLightCameraHelper = new THREE.CameraHelper( pointLight.shadow.camera );
scene.add(pointLightCameraHelper);
// pointLightCameraHelper.visible = false;

/**
 * Resizer
 */
window.addEventListener('resize', () => {
  sizes.width = window.innerWidth;
  sizes.height = window.innerHeight;
  camera.aspect = sizes.width / sizes.height;
  camera.updateProjectionMatrix();
  renderer.setSize(sizes.width, sizes.height);
});
window.addEventListener('dblclick', () => {
  const fullscreenElement = document.fullscreenElement || document.webkitFullscreenElement;
  if(!fullscreenElement) {
    if(canvas.requestFullscreen) {
      canvas.requestFullscreen();
    } else if(canvas.webkitRequestFullscreen) {
      canvas.webkitRequestFullscreen();
    }
  } else {
    if(document.exitFullscreen) {
      document.exitFullscreen();
    } else if(document.webkitExitFullscreen) {
      document.webkitExitFullscreen();
    }
  }
});

/**
 * Camera
 */
const camera = new THREE.PerspectiveCamera(75, sizes.width / sizes.height);
camera.position.set(0,0,3);
scene.add(camera)
camera.lookAt(cube.position);

renderer.render(scene, camera);

const controls = new OrbitControls(camera, canvas);
controls.enableDamping = true;


/**
 * Animations
 */

let time = Date.now();
let clock = new THREE.Clock();

const tick = () => {
    const elapsedTime = clock.getElapsedTime();

    cube.position.x = Math.cos(elapsedTime) * 1.5;
    cube.position.z = Math.sin(elapsedTime) * 1.5;
    cube.position.y = Math.abs(Math.sin(elapsedTime * 3)) * 0.8;

    objShadow.position.x = Math.cos(elapsedTime) * 1.5;
    objShadow.position.z = Math.sin(elapsedTime) * 1.5;
    objShadow.material.opacity = (1 - cube.position.y) * 0.3;

    controls.update();

    renderer.render(scene, camera);

    window.requestAnimationFrame(tick);
};

tick();