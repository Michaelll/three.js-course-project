import './style.css';
import * as THREE from 'three';
import gsap from 'gsap';
import * as dat from 'dat.gui';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { random } from 'gsap/all';
import { VertexColors } from 'three';

const cursor = {
  x: 0,
  y: 0,
};
window.addEventListener('mousemove', (e) => {
  cursor.x = e.clientX / sizes.width - 0.5;
  cursor.y = -(e.clientY / sizes.height - 0.5);
})

// Canvas
const canvas = document.querySelector('canvas.webgl')

// Scene
const scene = new THREE.Scene()

const textureLoader = new THREE.TextureLoader();
const particleTexture = textureLoader.load('/textures/particles/2.png');

/**
 * Particles
 */
const particleGeometry = new THREE.BufferGeometry();
const particleMaterial = new THREE.PointsMaterial({
  size: 0.05,
  sizeAttenuation: true,
  // color: '#f148ca',
  transparent: true,
  alphaMap: particleTexture,
  // alphaTest: 0.001, // 1 решает баг с прозрачным фоном
  // depthTest: false, // 2 решает баг с прозрачным фоном
  depthWrite: false, // 3 решает баг с прозрачным фоном
  blending: THREE.AdditiveBlending, // влияет на производительность
  vertexColors: true,
});
const count = 20000;
const positions = new Float32Array(count*3);
const colors = new Float32Array(count*3);

for (let i = 0; i < count * 3; i++) {
  positions[i] = (Math.random() - 0.5) * 10;
  colors[i] = Math.random();
}

const cube = new THREE.Mesh(
  new THREE.BoxBufferGeometry(),
  new THREE.MeshBasicMaterial()
);
// scene.add(cube);



particleGeometry.setAttribute(
  'position',
  new THREE.BufferAttribute(positions, 3)
)
particleGeometry.setAttribute(
  'color',
  new THREE.BufferAttribute(colors, 3)
)

const particles = new THREE.Points(particleGeometry, particleMaterial);
scene.add(particles);

const pointGeo = new THREE.SphereBufferGeometry(0.05, 32, 32);


/**
 * Object
 */
const group = new THREE.Group();
scene.add(group);


/**
 * Sizes
 */
const sizes = {
    width: window.innerWidth,
    height: window.innerHeight
}

/**
 * Resizer
 */
window.addEventListener('resize', () => {
  sizes.width = window.innerWidth;
  sizes.height = window.innerHeight;
  camera.aspect = sizes.width / sizes.height;
  camera.updateProjectionMatrix();
  renderer.setSize(sizes.width, sizes.height);
});
window.addEventListener('dblclick', () => {
  const fullscreenElement = document.fullscreenElement || document.webkitFullscreenElement;
  if(!fullscreenElement) {
    if(canvas.requestFullscreen) {
      canvas.requestFullscreen();
    } else if(canvas.webkitRequestFullscreen) {
      canvas.webkitRequestFullscreen();
    }
  } else {
    if(document.exitFullscreen) {
      document.exitFullscreen();
    } else if(document.webkitExitFullscreen) {
      document.webkitExitFullscreen();
    }
  }
});

/**
 * Camera
 */
const camera = new THREE.PerspectiveCamera(75, sizes.width / sizes.height);
camera.position.set(0,0,3);
scene.add(camera)
camera.lookAt(0, 0, 0);

const controls = new OrbitControls(camera, canvas);
controls.enableDamping = true;

/**
 * Renderer
 */
const renderer = new THREE.WebGLRenderer({
    canvas: canvas
})
renderer.setSize(sizes.width, sizes.height);
renderer.render(scene, camera);
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));

/**
 * Animations
 */

let time = Date.now();
let clock = new THREE.Clock();

const tick = () => {
    const elapsedTime = clock.getElapsedTime();

    // particles.rotation.x = elapsedTime / 10;

    for (let i = 0; i < count; i++) {
      const i3 = i * 3;
      const x = particleGeometry.attributes.position.array[i3 + 0];
      particleGeometry.attributes.position.array[i3 + 1] = Math.sin(elapsedTime + x);
    }
    particleGeometry.attributes.position.needsUpdate = true;

    controls.update();

    renderer.render(scene, camera);

    window.requestAnimationFrame(tick);
};

tick();