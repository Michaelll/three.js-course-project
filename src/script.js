import './style.css';
import * as THREE from 'three';
import gsap from 'gsap';
import * as dat from 'dat.gui';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import * as CANNON from 'cannon-es';

console.log(CANNON);

const cursor = {
  x: 0,
  y: 0,
};

/**
 * Sizes
 */
 const sizes = {
  width: window.innerWidth,
  height: window.innerHeight
}

const gui = new dat.GUI();
const debugObject = {};
 
debugObject.createSphere = () => {
  createSphere(Math.random() * 0.5, {x: (Math.random() - 0.5) * 3, y: 7, z: (Math.random() - 0.5) * 3})
}
gui.add(debugObject, 'createSphere');

debugObject.createBox = () => {
  createBox(
    Math.random(),
    Math.random(),
    Math.random(),
     {
       x: (Math.random() - 0.5) * 3,
       y: 7,
       z: (Math.random() - 0.5) * 3
      }
  )
}
gui.add(debugObject, 'createBox');

debugObject.reset = () => {
  console.log('reset');
  for (const object of objectsToUpdate) {
    object.body.removeEventListener('collide', playHitSound);
    world.removeBody(object.body);
    scene.remove(object.mesh);
  }
}
gui.add(debugObject, 'reset');

// Canvas
const canvas = document.querySelector('canvas.webgl')

// Scene
const scene = new THREE.Scene()

const hitSound = new Audio('/sounds/hit.mp3');
const playHitSound = (collision) => {
  const impactStrength = collision.contact.getImpactVelocityAlongNormal();
  if (impactStrength > 1.5) {
    hitSound.volume = Math.random();
    hitSound.currentTime = 0;
    hitSound.play();
  }
};


/**
 * Camera
 */
 const camera = new THREE.PerspectiveCamera(75, sizes.width / sizes.height);
 camera.position.set(-5,3,20);
 scene.add(camera)
 camera.lookAt(0,0,0);
 
 const controls = new OrbitControls(camera, canvas);
 controls.enableDamping = true;
 
 /**
  * Renderer
  */
 const renderer = new THREE.WebGLRenderer({
     canvas: canvas
 })
 renderer.setSize(sizes.width, sizes.height);
 renderer.render(scene, camera);
 renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));
 renderer.shadowMapEnabled = true;
 renderer.shadowMapSoft = true;
 renderer.shadowMap.type = THREE.PCFSoftShadowMap;
 renderer.shadowMap.enabled = true; 


window.addEventListener('mousemove', (e) => {
  cursor.x = e.clientX / sizes.width - 0.5;
  cursor.y = -(e.clientY / sizes.height - 0.5);
})


const world = new CANNON.World();
world.gravity.set(0, -9.82, 0);
world.broadphase = new CANNON.SAPBroadphase(world); // for performance
world.allowSleep = true; // for performance

let objectsToUpdate = [];

const sphereGeometry = new THREE.SphereBufferGeometry(1, 40, 40);
const sphereMaterial = new THREE.MeshStandardMaterial({ 
  color: 0xe8582c,
  clearcoat: 1.0,
  cleacoatRoughness:0.1,
  roughness: 0.5,
  metalness: 0.5,
})
const createSphere = (radius, position) => {
  const mesh = new THREE.Mesh(
    sphereGeometry,
    sphereMaterial
  );
  mesh.scale.set(radius, radius, radius);
  mesh.position.copy(position);
  mesh.castShadow = true;
  scene.add(mesh);

  const shape = new CANNON.Sphere(0.5);
  const body = new CANNON.Body({
    mass: 1,
    position: new CANNON.Vec3(0,5.5,0),
    shape: shape,
    material: defaultMaterial,
  });
  body.applyLocalForce(new CANNON.Vec3(150,0,0), new CANNON.Vec3(0,0,0));
  body.position.copy(position);
  body.addEventListener('collide', playHitSound);
  world.addBody(body);

  //save in objects to update
  objectsToUpdate.push({
    mesh,
    body
  })
};


const boxGeometry = new THREE.BoxBufferGeometry(1, 1, 1);
const boxMaterial = new THREE.MeshStandardMaterial({ 
  color: 0x7c0fdb,
  clearcoat: 1.0,
  cleacoatRoughness:0.1,
  roughness: 0.5,
  metalness: 0.5,
})
const createBox = (width, height, depth, position) => {
  const mesh = new THREE.Mesh(
    boxGeometry,
    boxMaterial
  );
  mesh.scale.set(width, height, depth);
  mesh.position.copy(position);
  mesh.castShadow = true;
  scene.add(mesh);

  const shape = new CANNON.Box(new CANNON.Vec3(width / 2, height / 2, depth / 2)); //потому что по другому там устроено и делим на 2
  const body = new CANNON.Body({
    mass: 1,
    position: new CANNON.Vec3(0,5.5,0),
    shape: shape,
    material: defaultMaterial,
  });
  body.applyLocalForce(new CANNON.Vec3(150,0,0), new CANNON.Vec3(0,0,0));
  body.position.copy(position);
  body.addEventListener('collide', playHitSound);
  console.log(body);
  world.addBody(body);

  //save in objects to update
  objectsToUpdate.push({
    mesh,
    body
  })
};


// const concreteMaterial = new CANNON.Material('concrete');
// const plasticMaterial = new CANNON.Material('plastic');

// const concretePlasticContactMaterial = new CANNON.ContactMaterial(
//   concreteMaterial,
//   plasticMaterial,
//   {
//     friction: 0.1,
//     restitution: 0.7,
//   }
// );
// world.addContactMaterial(concretePlasticContactMaterial);

const defaultMaterial = new CANNON.Material('default');

const defaultContactMaterial = new CANNON.ContactMaterial(
  defaultMaterial,
  defaultMaterial,
  {
    friction: 0.1,
    restitution: 0.7,
  }
);
world.addContactMaterial(defaultContactMaterial);

// const sphereShape = new CANNON.Sphere(0.5);
// const sphereBody = new CANNON.Body({
//   mass: 1,
//   position: new CANNON.Vec3(0,5.5,0),
//   shape: sphereShape,
//   material: defaultMaterial,
// });
// sphereBody.applyLocalForce(new CANNON.Vec3(150,0,0), new CANNON.Vec3(0,0,0));
// world.addBody(sphereBody);

const floorShape = new CANNON.Plane();
const floorBody = new CANNON.Body({
  mass: 0,
  material: defaultMaterial,
});
floorBody.addShape(floorShape);
floorBody.quaternion.setFromAxisAngle(new CANNON.Vec3(1,0,0), -Math.PI / 2);
world.addBody(floorBody);
/**
 * Object
 */
const textureloader = new THREE.TextureLoader();
const texture = textureloader.load('/textures/environmentMaps/3/nx.jpg');

const group = new THREE.Group();
scene.add(group);

// const sphereGeometry = new THREE.SphereBufferGeometry(1, 40, 40);
// const sphereMaterial = new THREE.MeshStandardMaterial({ 
//   color: 0xe8582c,
//   clearcoat: 1.0,
//   cleacoatRoughness:0.1,
//   roughness: 0.5,
//   metalness: 0.5,
//   envMap: texture,
//   envMapIntensity: 1.0,
//  })
// const sphere = new THREE.Mesh(sphereGeometry, sphereMaterial);
// sphere.scale.set(0.5,0.5,0.5);
// sphere.position.set(0,2.5,0);
// sphere.castShadow = true;
// scene.add(sphere);

const planeGeometry = new THREE.PlaneBufferGeometry(10, 10, 10);
const planeMaterial = new THREE.MeshStandardMaterial({ 
  color: 0x7d828a,
  cleacoatRoughness:0.1,
  roughness: 0.8,
  metalness: 0.2,
 })
const plane = new THREE.Mesh(planeGeometry, planeMaterial);
plane.rotation.x = -Math.PI / 2;
plane.receiveShadow = true;
scene.add(plane);

const ambientlLight = new THREE.AmbientLight(0xffffff, 0.5);
scene.add(ambientlLight);

const directionalLight = new THREE.DirectionalLight(0xffffff, 0.8);
directionalLight.position.set(10,10,10);
directionalLight.castShadow = true;
scene.add(directionalLight);

directionalLight.shadow.mapSize.width = 1024;
directionalLight.shadow.mapSize.height = 1024;


createSphere(.5, {x: 0, y: 5, z: 0});
createSphere(.7, {x: 0, y: 5, z: -3});
console.log(objectsToUpdate);

/**
 * Resizer
 */
window.addEventListener('resize', () => {
  sizes.width = window.innerWidth;
  sizes.height = window.innerHeight;
  camera.aspect = sizes.width / sizes.height;
  camera.updateProjectionMatrix();
  renderer.setSize(sizes.width, sizes.height);
});
// window.addEventListener('dblclick', () => {
//   const fullscreenElement = document.fullscreenElement || document.webkitFullscreenElement;
//   if(!fullscreenElement) {
//     if(canvas.requestFullscreen) {
//       canvas.requestFullscreen();
//     } else if(canvas.webkitRequestFullscreen) {
//       canvas.webkitRequestFullscreen();
//     }
//   } else {
//     if(document.exitFullscreen) {
//       document.exitFullscreen();
//     } else if(document.webkitExitFullscreen) {
//       document.webkitExitFullscreen();
//     }
//   }
// });


/**
 * Animations
 */

let time = Date.now();
let clock = new THREE.Clock();
let oldElapsedTime = 0;

const tick = () => {
    const elapsedTime = clock.getElapsedTime();
    const deltaTime = elapsedTime - oldElapsedTime;
    oldElapsedTime = elapsedTime;

    //update physics world
    world.step(1/ 60, deltaTime, 3);
  
    objectsToUpdate.forEach(element => {
      element.mesh.position.copy(element.body.position);
      element.mesh.quaternion.copy(element.body.quaternion);
    });
    // sphere.position.copy(sphereBody.position); //the same
    // sphere.position.x = sphereBody.position.x;
    // sphere.position.y = sphereBody.position.y;
    // sphere.position.z = sphereBody.position.z;

    // sphereBody.applyForce(new CANNON.Vec3(-0.5,0,0), sphereBody.position);

    controls.update();

    renderer.render(scene, camera);

    window.requestAnimationFrame(tick);
};

tick();